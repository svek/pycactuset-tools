#!/usr/bin/env python
# Python2 using PyCactusET https://bitbucket.org/DrWhat/pycactuset

"""
Displays whats inside one or multiple given CarpetHDF5 files. By default,
is very brief. See the arguments for more information. The script basically
displays a string version of postcactus's internal cactus_grid_h5, part of
the PyCactusET.

Note that reading already multiple 2D files can be slow. Consider exploiting
parallelism on an OS level, for instance instead of calling

$ cactus-h5-whats-inside.py  foo*.h5

call

$ for x in foo*.h5; do cactus-h5-whats-inside.py $x > $x.struct.txt & done; wait
$ cat *.struct.txt

"""

# python included
import argparse, os

parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("files", metavar="something.h5", nargs='+', help="One or multiple CarpetHDF5 files")
parser.add_argument("--all-iters", action="store_true", help="Display infos for all iterations")
args = parser.parse_args()

# needs PyCactusET in pythonpath
from postcactus import cactus_grid_h5

indent = lambda level: "  "*level
for fname in args.files:
	f = cactus_grid_h5.GridH5File(fname)
	print "File %s, %d iterations" % (fname, len(f.get_iters()))
	for it in f.get_iters():
		print indent(1) + "Iteration %d, %d reflevels" % (it, len(f.get_it_levels(it)))
		for field in f.get_fields():
			print indent(2) + "Field %s, Time %s:" % (field, f.read_time(field,it))
			for level in f.get_it_levels(it):
				sp = f.read_spacing(field, it, level)
				print indent(3) + "Level %d, Spacing: %s x %s" % (level, str(sp[0]), str(sp[1]))
		if not args.all_iters:
			break

print "Done."